var chai = require('chai');
var chaiHttp = require('chai-http');
const expect = require('chai').expect;


chai.use(chaiHttp);

const url= 'http://localhost:4300';

var data1 = require('./test_01.json');
var data2 = require('./test_02.json');



describe('API works', ()=>{

	it('you can access to ' + url, (done) => {
		chai.request(url)
			.get('/')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});



describe('Insert new weather data', ()=>{

	it('Insert new weather data', (done) => {
		chai.request(url)
			.post('/weather')
			.send(data1)
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});

describe('Retrieve all data weather', ()=>{

	it('should insert a country', (done) => {
		chai.request(url)
			.get('/weather')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});


describe('Retrieve all data weather of one city', ()=>{

	it('should insert a country', (done) => {
		chai.request(url)
			.get('/weather/barcelona')
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});

describe('Delete one weather data', ()=>{

	it('try to del the id sent', (done) => {
		chai.request(url)
			.delete('/weather')
			.send(data2)
			.end( function(err,res){
				console.log(res.body)
				expect(res).to.have.status(200);
				done();
			});
	});
});
