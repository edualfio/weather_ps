var queryObj = {
	getAll: 'SELECT weather.*, locations.city as city, locations.state as state, locations.lan as lan, locations.lon as lon FROM weather INNER JOIN locations ON locations.id = weather.location',
	getByCity: 'SELECT weather.*, locations.city as city, locations.state as state, locations.lan as lan, locations.lon as lon FROM weather INNER JOIN locations ON locations.id = weather.location WHERE locations.city = ?',
	insertLocation: 'INSERT INTO locations SET ?',
	checkCity: 'SELECT id FROM locations WHERE city=?',
	insertWeather: 'INSERT INTO weather SET ?',
	deleteWeather: 'DELETE FROM weather WHERE id=?'
}

module.exports = queryObj;
