require('dotenv').config();
var express = require('express');
var app = express();
var bodyParser = require('body-parser');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));


var home = require('./controller/home.js');
var weather = require('./controller/weather.js');

// routes
app.use('/', home);
app.use('/weather', weather);


module.exports = app;



