
# API weather PS

## prev
import sql file with test dummy data
edit .env file to set ports, and database connection

## init
run ```npm install```
then ```npm start```

## test
5 tests, done with mocha and chai, run ```npm test```


## endpoints

**GET /weather/**

**GET /weather/:city**

**POST /weather/**

Data example:

>     {	
>         "location": {
>             "city": "Manresa",
>             "state": "Barcelona",
>             "lan": 232345,
>             "lon": 3452345
>         },
>         "temps": [
>             "2",
>             "42",
>             "17",
>             "36",
>             "1"
>         ],
>         "date": "2018-11-12"
>         "weather": "sunny"
>     }


**DELETE /weather/**

>     {	
>         "location": {
>             "city": "Manresa",
>             "state": "Barcelona",
>             "lan": 232345,
>             "lon": 3452345
>         },
>         "temps": [
>             "2",
>             "42",
>             "17",
>             "36",
>             "1"
>         ],
>         "date": "2018-11-12"
>         "weather": "sunny"
>     }


