var express = require('express');
var router = express.Router();
var async = require('async');

var middlewares = require('../middlewares');

var Weather = require('../connect/weather');
var queryObj = require('../querys');



router.get('/:city?', function(req,res,next){

	var actualQuery;

	// recogemos parametro de la url para modificar la consulta
	if(req.params.city) {
		actualQuery =  queryObj.getByCity;
	} else {
		actualQuery = queryObj.getAll;
	}

	middlewares.retrieveData(actualQuery, req.params.city, function(result, weather){
		if (!result) {
			res.status(500).send({message: 'Error at query'});

		} else {

			if(weather.length > 0 ) {
				res.status(200).send({ weather });

			} else {
				res.status(200).send({ message: 'No data for query' });

			}


		}
	})

});


router.post('/', function(req,res,next){
	var values = JSON.parse(JSON.stringify(req.body));
	var idLocation;

	// Iniciamos un proceso asyncrono para insertar primero la ciudad, resolver la id generada por la consulta, y almacenarla en la variable para la viculación con el dato. Si ya existe dicha localización, lastId recoge la id de ciudad y la almacena global.
	async.series([
		function(callback){

			middlewares.insertDataLocation(queryObj.checkCity, values, function(idAdded){
				idLocation = idAdded;
				callback(null);
			})

		},
		function(callback){
			// Una nueva variable será la encargada de manejar los arraysw y las fechas para su inclusión final en la tabla weather.
			let newWeather = {
				'location': idLocation,
				'temps': values.temps.join(','),
				'date': new Date(values.date),
				'weather': values.weather
			}

			middlewares.insertDataWeather(queryObj.insertWeather, newWeather, function(result){
				if (!result) {
					res.status(500).send({message: 'No info at db'});
				} else {
					res.status(200).send({message: 'Data added'});
				}
			})

		}
		]);

});

router.delete('/', function(req,res,next){
	var values = JSON.parse(JSON.stringify(req.body));

	middlewares.insertDataWeather(queryObj.deleteWeather, values.id, function(result, weather){
		if (!result) {
			res.status(500).send({data:err, message: 'Upps something was wrong'});
		} else {
			res.status(200).send({ message: 'Data deleted. Id deleted: ' + values.id});
		}
	})

});


module.exports = router;

