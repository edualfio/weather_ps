# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.38)
# Base de datos: weather_ps
# Tiempo de Generación: 2018-10-20 19:51:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `lan` int(11) DEFAULT NULL,
  `lon` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;

INSERT INTO `locations` (`id`, `city`, `state`, `lan`, `lon`)
VALUES
	(1,'Sabadell','Barcelona',232345,3452345),
	(2,'Barcelona','Barcelona',232345,3452345),
	(3,'Manresa','Barcelona',232345,3452345);

/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla weather
# ------------------------------------------------------------

DROP TABLE IF EXISTS `weather`;

CREATE TABLE `weather` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `location` varchar(40) DEFAULT NULL,
  `temps` text,
  `date` date DEFAULT NULL,
  `weather` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `weather` WRITE;
/*!40000 ALTER TABLE `weather` DISABLE KEYS */;

INSERT INTO `weather` (`id`, `location`, `temps`, `date`, `weather`)
VALUES
	(2,'2','23.4,23.3,14.5','2018-10-20','cloudy'),
	(4,'2','56,23,12,24,34.5','2018-10-20','sunny'),
	(5,'3','0,12,17,30,34.5','2018-10-20','snow'),
	(6,'3','2,42,17,36,1','2018-10-20','sunny'),
	(7,'3','2,42,17,36,1','2018-11-12','sunny'),
	(8,'3','2,42,17,36,1','2018-11-12','sunny'),
	(10,'3','2,42,17,36,1','2018-11-12','sunny'),
	(11,'3','2,42,17,36,1','2018-11-12','sunny'),
	(12,'3','2,42,17,36,1','2018-11-12','sunny'),
	(13,'3','2,42,17,36,1','2018-11-12','sunny'),
	(14,'3','2,42,17,36,1','2018-11-12','sunny');

/*!40000 ALTER TABLE `weather` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
