var Weather = require('../connect/weather');
var queryObj = require('../querys');

module.exports = {
	retrieveData: function (query, param, cb) {

		Weather.query(query, param, (err, result) => {

			if (err) {

				cb(false);

			} else {

				// modificamos el objeto resultante para acomodar los datos de localización vinculados al dato
				let weather = [];

				result.forEach((resNew, index) => {

					let newResult = {
						'id': resNew.id,
						'location': {
							'city': resNew.city,
							'state': resNew.state,
							'lan': resNew.lan,
							'lon': resNew.lon
						},
						'temps': resNew.temps.split(','),
						'date': resNew.date,
						'weather': resNew.weather
					}

					weather.push(newResult);
				});

				cb(true, weather);

			}

		});
	},
	insertDataLocation: function (query, params, cb) {

		Weather.query(query, params.location.city, (err, result) => {

			if (result.length > 0) {
				lastId = result[0].id;
				cb(lastId);

			} else {
				Weather.query(queryObj.insertLocation, params.location, (err, result) => {
					lastId = result.insertId;
					cb(lastId);
				});
			}


		});
	},
	insertDataWeather: function (query, values, cb) {

		Weather.query(query, values, (err, result) => {

			if (err) {
				cb(false);

			} else {
				cb(true);

			}

		});
	},
	deleteDataWeather: function (query, param) {

		Weather.query(query, param, (err, result) => {

			if (err) {
				cb(false);

			} else {
				cb(true, result);

			}

		});


	}


}
