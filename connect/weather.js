var mysql = require('mysql')
var config = require('../config/config.js');


var model = mysql.createPool({
	connectionLimit : 10,
	host     : config.mysql.host,
	user     : config.mysql.user,
	password : config.mysql.password,
	database : config.mysql.database,
	port: config.mysql.port
})


module.exports = model;
